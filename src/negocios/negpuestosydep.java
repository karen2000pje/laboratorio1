/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocios;

import datos.archivos;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import objetos.objetosEmplead;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/**
 *
 * @author Usuario
 */
public class negpuestosydep {

    archivos arch = new archivos();

    public ArrayList<String> llamapuestos() {
        ArrayList guardainfopuest = arch.LeerDesdeArchivopues();
        ArrayList<String> Arregpuest = new ArrayList<>();
        for (int i = 0; i < guardainfopuest.size(); i++) {
            String[] list = guardainfopuest.get(i).toString().split(",");
            Arregpuest.add(list[0]);
            Arregpuest.add(list[1]);
            Arregpuest.add(list[2]);
            Arregpuest.add(list[3]);

        }
        return Arregpuest;
    }

    public String[][] extraepues() {
        ArrayList<String> lista_puestos = arch.LeerDesdeArchivopues();
        String[][] datoMatriz1 = new String[lista_puestos.size()][4];
        for (int i = 0; i < lista_puestos.size(); i++) {
            String valor[] = lista_puestos.get(i).split(",");
            for (int j = 0; j <= 3; j++) {
                datoMatriz1[i][j] = valor[j];
            }
        }
        return datoMatriz1;
    }
    /////////// 

    public void InsertarPersonas(ArrayList<objetosEmplead> listanewempleados) {
        String datos = "";
        for (int i = 0; i <= listanewempleados.size() - 1; i++) {
            int cedula = listanewempleados.get(i).getCedula();
            String nombre = listanewempleados.get(i).getNombre();
            String genero = listanewempleados.get(i).getGenero();
            String depa = listanewempleados.get(i).getDepartamento();
            String fechnaci = listanewempleados.get(i).getFechanaci();
            String edad = listanewempleados.get(i).getEdad();
            String fechingre = listanewempleados.get(i).getFechaingres();
            String nivelIngle = listanewempleados.get(i).getNivelingles();
            String puesto = listanewempleados.get(i).getPuesto();

            datos = cedula + "," + nombre + "," + genero + "," + depa + "," + fechnaci + "," + edad + "," + fechingre + "," + nivelIngle + "," + puesto;

        }
        arch.InsertarenArchivoempe(datos);
    }

    public static String edad(Date fecha_naci) {//metodo para calcular la edad
        LocalDate fecha_nacimientoLD = DateUtils.asLocalDate(fecha_naci);
        LocalDate hoy = LocalDate.now();
        Period periodo = Period.between(fecha_nacimientoLD, hoy);
        String an = (periodo.getYears() == 1) ? "año" : "años";
        String me = (periodo.getMonths() == 1) ? "mes" : "meses";
        String di = (periodo.getDays() == 1) ? "día" : "días";
        return periodo.getYears() + an + periodo.getMonths() + me + periodo.getDays() + di;
    }

    public static Date ParseFecha(String fecha) {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } catch (ParseException ex) {
            System.out.println(ex);
        }
        return fechaDate;
    }

    public String[][] extraeperso() {
        ArrayList<String> lista_personas = arch.LeerDesdeArchivoempe();
        String[][] datoMatriz1 = new String[lista_personas.size()][9];
        for (int i = 0; i < lista_personas.size(); i++) {
            String valor[] = lista_personas.get(i).split(",");
            for (int j = 0; j <= 8; j++) {
                datoMatriz1[i][j] = valor[j];
            }
        }
        return datoMatriz1;
    }

    public ArrayList<String> llamadepart() {
        ArrayList guardainfodep = arch.LeerDesdeArchivodep();
        ArrayList<String> Arregdep = new ArrayList<>();
        for (int i = 0; i < guardainfodep.size(); i++) {
            String[] list = guardainfodep.get(i).toString().split(",");
            Arregdep.add(list[0]);
            Arregdep.add(list[1]);
            Arregdep.add(list[2]);
        }
        return Arregdep;
    }

    public String[][] extraedepa() {
        ArrayList<String> lista_depa = arch.LeerDesdeArchivodep();
        String[][] datoMatriz1 = new String[lista_depa.size()][3];
        for (int i = 0; i < lista_depa.size(); i++) {
            String valor[] = lista_depa.get(i).split(",");
            for (int j = 0; j <= 2; j++) {
                datoMatriz1[i][j] = valor[j];
            }
        }
        return datoMatriz1;

    }

    public ArrayList llamarIDRutas() {
        ArrayList id = arch.LeerDesdeArchivoempe();
        ArrayList enviarInfo = new ArrayList();

        for (int i = 0; i < id.size(); i++) {
            String conver = (String) id.get(i);
            String[] llamar = conver.split(",");
            if (!enviarInfo.contains(llamar[2])) {
                enviarInfo.add(llamar[2]);
            }

        }
        return enviarInfo;
    }
}
