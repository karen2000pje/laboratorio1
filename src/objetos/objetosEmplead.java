/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Usuario
 */
public class objetosEmplead {

    public static ArrayList getListanewempleados() {
        return listanewempleados;
    }

    public static void setListanewempleados(ArrayList listanewempleados) {
        objetosEmplead.listanewempleados = listanewempleados;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getFechanaci() {
        return fechanaci;
    }

    public void setFechanaci(String fechanaci) {
        this.fechanaci = fechanaci;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getFechaingres() {
        return fechaingres;
    }

    public void setFechaingres(String fechaingres) {
        this.fechaingres = fechaingres;
    }

    public String getNivelingles() {
        return nivelingles;
    }

    public void setNivelingles(String nivelingles) {
        this.nivelingles = nivelingles;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public objetosEmplead(int cedula, String nombre, String genero, String departamento, String fechanaci, String edad, String fechaingres, String nivelingles, String puesto) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.genero = genero;
        this.departamento = departamento;
        this.fechanaci = fechanaci;
        this.edad = edad;
        this.fechaingres = fechaingres;
        this.nivelingles = nivelingles;
        this.puesto = puesto;
    }

    public static ArrayList listanewempleados = new ArrayList<>();//lista objetos
    private int cedula;
    private String nombre;
    private String genero;
    private String departamento;
    private String fechanaci;
    private String edad;
    private String fechaingres;
    private String nivelingles;
    private String puesto;

}
